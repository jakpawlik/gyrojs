var $ = jQuery.noConflict();

(function ($) {

    $.gyro = {
        options: {
            target: '#app-container',
            method: 'GET',
            menu: 'ul.nav',
            menuElement: 'li',
            activeClass: 'active',
            loadingClass: '.gyro-loading',
            loaderElement: $('<span class="gyro-loading fa fa-spin fa-cog"></span>'),
            ext: false,
            ignore: '.no-gyro',
            defaultSelector: 'a, form',
            callback: function (target, response, url, options) {

                target.html(response);

                target.find(options.defaultSelector).runGyro(options);

            }
        },
        getDate: function(data){

            var data = new Date(data);

            return data.getFullYear() + '-' + ( (data.getMonth() + 1) < 10 ? 0 : '' ) + (data.getMonth() + 1)  + '-' + data.getDate()
        },
        matchUrl: function (url, href){

            href = href.replace(/\//g,'\\/');

            var regex = new RegExp(href + '$', 'g');

            var result = url.search(regex);

            return result > -1 ? true : false;
        },
        history: {
            currentIndex: null,
            urls: [],
            add: function (url, title, method, data, options) {

                window.history.pushState('Object', title, url);

                this.urls.push({url: url, method: method, data: data, options: options});

                if (typeof _qaq !== 'undefined') {
                    _gaq.push(['_trackPageview', url]);
                }

                this.currentIndex = this.urls.length - 1;

            },
            find: function (url) {

                var prevPos = this.currentIndex - 1;

                var nextPos = this.currentIndex + 1;

                var result = -1;


                if (prevPos >= 0) {

                    var prevObj = this.urls[prevPos];

                    if ($.gyro.matchUrl(url, prevObj.url)) {
                        result = prevPos;
                    }

                }

                if (nextPos <= this.urls.length - 1) {

                    var nextObj = this.urls[nextPos];

                    if ($.gyro.matchUrl(url, nextObj.url)) {
                        result = nextPos;
                    }
                }

                return result;

            },
            walk: function (index) {

                if (index === null || index < 0 || index > this.urls.length - 1) {
                    return false;
                }

                var historyPos = this.urls[index];

                var options = historyPos.options;

                options.ext = true;

                this.currentIndex = index;

                $.gyro.send(historyPos.url, historyPos.method, historyPos.data, options);

            }
        },
        send: function (url, method, data, options) {

            data = $.gyro.get(data, {});

            if (options.menu) {

                $(options.menu).each(function () {

                    var currentMenu = $(this);

                    currentMenu.find(options.menuElement).each(function () {

                        var currentMenuElement = $(this);
                        currentMenuElement.removeClass(options.activeClass);

                        var href = currentMenuElement.find('a').gyroGet('href');

                        var check = $.gyro.matchUrl(url, href);

                        if (check) {
                            currentMenuElement.addClass(options.activeClass);
                        }

                    });

                });

            }

            options.loaderElement.fadeOut(0, function () {

                $('body').append(options.loaderElement);

                options.loaderElement.fadeIn('slow');
            });

            $.ajax({
                url: url,
                data: data,
                type: method,
                success: function (response) {

                    if (!options.ext) {
                        var jResponse = $('<div>' + response + '</div>');

                        var title = jResponse.find('h1').length ? $.trim(jResponse.find('h1').text()) : ( jResponse.find('h2').length ? $.trim(jResponse.find('h2').text) : null);

                        if (title) {
                            document.title = title;
                        }

                        $.gyro.history.add(url, title, method, data, options);

                    }

                    var target = $(options.target);

                    options.callback(target, response, url, options);

                }
            })
        },
        get: function (element, defaultValue) {
            return typeof element === 'undefined' ? (typeof defaultValue === 'undefined' ? false : defaultValue) : element;
        }
    }

    $.fn.gyroGet = function (attr, defaultValue) {

        defaultValue = $.gyro.get(defaultValue);

        if (!$.gyro.get(this.attr(attr))) {

            if ($.gyro.get(this.data(attr))) {
                return this.data(attr);
            }

            return defaultValue ? defaultValue : null;

        }
        else {
            return this.attr(attr);
        }
    };

    $.fn.runGyro = function (options) {

        options = $.extend($.gyro.options, options);

        var _elements = this;

        _elements.not(options.ignore).each(function () {

            var _element = $(this);

            var ext = _element.gyroGet('gyro-ext', false);

            options.ext = ext;


            if (_element.is('form')) {

                _element.on('submit', function (e) {
                    e.preventDefault();

                    var _form = $(this);

                    if(_form.gyroGet('gyro-ext')){
                        options.ext = true;
                    }

                    var url = _form.gyroGet('action', window.location.href);
                    var method = _form.gyroGet('method', options.method);
                    //var data = new FormData(_form[0]);
                    var data = _form.serialize();

                    $.gyro.send(url, method, data, options);

                });

            } else {

                _element.on('click', function (e) {
                    e.preventDefault();

                    var _link = $(this);

                    var url = _link.gyroGet('href');

                    if(_link.gyroGet('gyro-ext')){
                        options.ext = true;
                    }

                    if (url.substr(0, 1) === '#') {
                        return;
                    }

                    var method = _link.gyroGet('method', options.method);

                    $.gyro.send(url, method, {}, options);

                });

            }
        });
    };

}(jQuery));

$(window).bind('statechange', function (e) {

    e.preventDefault();

    var State = History.getState();

    var index = $.gyro.history.find(State.url);

    var overrideOptions = {};

    if(typeof __gyro_history_ovverrideOptions !== 'undefined'){
        overrideOptions = __gyro_history_ovverrideOptions;
    }

    var options = $.extend($.gyro.options, overrideOptions);

    if (index === -1 || !$(options.target).length ){
        window.location = State.url;
    }else{
        $.gyro.history.walk(index);

    }



});